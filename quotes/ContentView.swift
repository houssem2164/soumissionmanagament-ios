//projet fait par houssem aloui et maryem ben salem

import SwiftUI

struct ContentView: View
{
    @State var quotesList = Array<Quote>()
    @State var sortedList = Array<Quote>()
    
    @State var sortByNumber = false
    
    var body: some View
    {
        NavigationView
        {
            VStack
            {
                if !quotesList.isEmpty
                {
                    Button {
                        sortByNumber.toggle()
                    } label: {
                        Text("Trier par : \(sortByNumber ? "active" : "Desactive")")
                    }

                    List
                    {
                        if sortByNumber
                        {
                            ForEach((0...sortedList.count-1), id: \.self) {index in
                                NavigationLink(destination: QuoteDetailView(quote: $sortedList[index], quotesList: $sortedList, index: index), label: {
                                    QuoteCellView(quote: sortedList[index])
                                })
                            }
                        }
                        else
                        {
                            ForEach((0...quotesList.count-1), id: \.self) {index in
                                NavigationLink(destination: QuoteDetailView(quote: $quotesList[index], quotesList: $quotesList, index: index), label: {
                                    QuoteCellView(quote: quotesList[index])
                                })
                            }
                        }
                    }
                    .listStyle(.plain)
                }
                else
                {
                    Text("Veuillez ajouter des soumissions")
                        .font(.system(size: 25))
                        .foregroundColor(.blue)
                }
            }
            .onAppear{
                sortedList = quotesList.sorted(by: { $0.price > $1.price })
            }.navigationTitle("Le Mieux Soumission") .foregroundColor(.blue)                .navigationBarTitleDisplayMode(.inline)
            .toolbar {
                ToolbarItem(placement: .navigationBarTrailing) {
                    NavigationLink(destination: QuoteAddView(quotesList: $quotesList), label: {
                        Text("Ajouter")
                            .foregroundColor(.blue)
                    })
                }
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider
{
    static var previews: some View
    {
        ContentView()
    }
}
