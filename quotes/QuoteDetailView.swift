//projet fait par houssem aloui et maryem ben salem


import SwiftUI
import MapKit

struct QuoteDetailView: View
{
    @Binding var quote: Quote
    @Binding var quotesList: Array<Quote>
    var index: Int
    
    @State var region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: 0, longitude: 0
                                                                         ), span: MKCoordinateSpan(latitudeDelta: 0.2, longitudeDelta: 0.2))
    
    @State var quoteLocation = [Location(location: CLLocationCoordinate2D(latitude: 0, longitude: 0))]
    
    @Environment(\.dismiss) var dismiss
    
    var body: some View
    {
        VStack
        {
            Map(coordinateRegion: $region, annotationItems: quoteLocation) { location in
                MapMarker(coordinate: location.location)
            }
            .frame(width: .infinity, height: 200)
            
            List
            {
                Section(header: Text("Projet")) {
                    TextField("Numero", text: $quote.number)
                    TextField("Nom", text: $quote.projectName)
                    TextField("Prix", text: $quote.price)
                    TextField("Adresse de soumissions", text: $quote.quoteAddress)
                    TextField("Ville de soumission", text: $quote.quoteCity)
                }

                Section(header: Text("Client")) {
                    TextField("Nom", text: $quote.customerName)
                    TextField("Address ", text: $quote.customerAddress)
                    TextField("Ville", text: $quote.customerCity)
                    TextField("Telephone", text: $quote.customerPhone)
                    TextField("Email", text: $quote.customerEmail)
                }
            }
            
                Button {
                    quotesList.remove(at: index)
                    dismiss()
                } label: {
                    Text("Supprimer")
                        .font(.system(size: 25))
                        .padding(.horizontal, 30)
                        .padding(.vertical, 10)
                        .background(.red)
                        .foregroundColor(.white)
                        .cornerRadius(10)
                }

        }
        .background(Color("LightGray"))
        .onAppear(perform: LocationFromName)
        .navigationBarTitleDisplayMode(.inline)
    }
    
    func LocationFromName() -> Void
    {
        let searchRequest = MKLocalSearch.Request()
        searchRequest.naturalLanguageQuery = "\(quote.quoteAddress), \(quote.quoteCity)"
        let search = MKLocalSearch(request: searchRequest)
        
        searchRequest.region = region
        search.start { response, error in
            guard let response = response else
            {
                print("Error: \(error?.localizedDescription ?? "Erreur Inconnu").")
                return
            }
            
            let locationResult = response.mapItems[0].placemark.coordinate

            quoteLocation.removeAll()
            quoteLocation.append(Location(location: locationResult))
            region = MKCoordinateRegion(center: locationResult, span: MKCoordinateSpan(latitudeDelta: 0.2, longitudeDelta: 0.2))
        }
    }
}

struct Location: Identifiable
{
    var id = UUID()
    var location: CLLocationCoordinate2D
}
