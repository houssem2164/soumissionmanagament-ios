//projet fait par houssem aloui et maryem ben salem 

import SwiftUI

struct QuoteAddView: View
{
    @Binding var quotesList: Array<Quote>
    @Environment(\.dismiss) var dismiss
    
    @State var number = ""
    @State var projectName = ""
    @State var price = ""
    @State var quoteAddress = ""
    @State var quoteCity = ""
    
    @State var customerName = ""
    @State var customerAddress = ""
    @State var customerCity = ""
    @State var customerPhone = ""
    @State var customerEmail = ""
    
    var body: some View
    {
        VStack
        {
            List
            {
                Section(header: Text("Projet")) {
                    TextField("Numero", text: $number)
                    TextField("Nom", text: $projectName)
                    TextField("Prix", text: $price)
                    TextField("Adresse soumission", text: $quoteAddress)
                    TextField("Ville soumission", text: $quoteCity)
                }
                
                Section(header: Text("Client")) {
                    TextField("Nom", text: $customerName)
                    TextField("Addresse ", text: $customerAddress)
                    TextField("Ville", text: $customerCity)
                    TextField("Telephone", text: $customerPhone)
                    TextField("Email", text: $customerEmail)
                }
            }
            
            Button {
                quotesList.append(Quote(number: number, projectName: projectName, price: price, quoteAddress: quoteAddress, quoteCity: quoteCity, customerName: customerName, customerAddress: customerAddress, customerCity: customerCity, customerPhone: customerPhone, customerEmail: customerEmail))
                dismiss()
            } label: {
                Text("Ajouter")
                    .font(.system(size: 25))
                    .padding(.horizontal, 30)
                    .padding(.vertical, 10)
                    .background(.blue)
                    .foregroundColor(.white)
                    .cornerRadius(10)
            }
        }
        .background(Color("LightGray"))
        .navigationBarTitleDisplayMode(.inline)
    }
}
