//projet fait par houssem aloui et maryem ben salem 

import SwiftUI

struct QuoteCellView: View
{
    let quote: Quote
    
    var body: some View
    {
            HStack
            {
                Text("#\(quote.number)")
                    .font(.system(size: 35))
                    .bold()
                    .padding(.leading,10)
                
                VStack(alignment: .leading)
                {
                    Text("Projet: \(quote.projectName)")
                        .font(.system(size: 20, weight: .bold))
                    Text("Client: \(quote.customerName)")
                        .font(.system(size: 20, weight: .bold))
                }
                
                Spacer()
                
                Text(("$\(quote.price)"))
                    .font(.system(size: 25, weight: .bold))
                    .padding(.trailing,10)
            }
        .frame(width: 400)        
        .background(.blue)
        .foregroundColor(.white)
        .cornerRadius(10)
    }
}



struct QuoteCellView_Previews: PreviewProvider
{
    static var previews: some View
    {
        QuoteCellView(quote: Quote(number: "0", projectName: "faire le plancher ", price: "700",quoteAddress: "73 rue eddy ", quoteCity: "Gatineau", customerName: "houssem aloui", customerAddress: "32 rue marier", customerCity: "ottawa", customerPhone: "513888888", customerEmail: "HHHH"))
    }
}
