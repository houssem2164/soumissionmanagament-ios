//projet fait par houssem aloui et maryem ben salem 

import Foundation

struct Quote
{
    var number: String
    var projectName: String
    var price: String
    var quoteAddress: String
    var quoteCity: String
    var customerName: String
    var customerAddress: String
    var customerCity: String
    var customerPhone: String
    var customerEmail: String
}
